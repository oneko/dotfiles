{ pkgs }:

with pkgs; [
	zoxide
	bat
	emacs
        ripgrep
        ffmpeg
        wget
        neofetch
        httpie
        freshfetch
        jq
]
