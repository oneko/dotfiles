;;; init-git.el --- For git.  -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:
(when (maybe-require-package 'magit)
  ())

(provide 'init-git)
;;; init-git.el ends here
