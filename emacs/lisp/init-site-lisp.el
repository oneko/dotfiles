;;; init-site-lisp.el --- Settings for non-elpa packages.  -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(add-to-list 'load-path (expand-file-name "site-lisp" user-emacs-directory))

;; (require 'awesome-tab)
;; (when (display-graphic-p)
;;   (awesome-tab-mode t))

;; (require 'nano-modeline)
;; (nano-modeline)

(provide 'init-site-lisp)
;;; init-site-lisp.el ends here
