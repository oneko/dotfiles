;;; init-haskell.el --- For programming haskell with emacs.  -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(when (maybe-require-package 'haskell-mode)
  )

(provide 'init-haskell)
;;; Local Variable:
;;; coding: utf-8
;;; End:
;;; init-haskell.el ends here
