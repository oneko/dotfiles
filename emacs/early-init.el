;;; early-init.el --- Init before init.el.  -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(setq package-enable-at-startup nil)

(provide 'early-init)
;;; Local Variables:
;;; coding: utf-8
;;; End:
;;; early-init.el ends here
